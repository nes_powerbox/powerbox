﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowerBox2.comands
{
    class CurrentComand : Comand
    {
        private const byte CURRENT = 5;
        private const byte SIZE = 1;
        private int current;

        public CurrentComand(byte currentLow, byte currentHigh )
        {
            this.current =  currentLow + (currentHigh * 255);
        }

        public CurrentComand() { }

        public override byte[] getComand()
        {
            byte[] bytes = new byte[2];
            bytes[0] = CURRENT;
            return bytes;
        }

        public override byte getSizeComand()
        {
            return SIZE;
        }

        public int getCurrent()
        {
            return current;
        }

        public override string ToString()
        {
            return "CurrentComand{current: " + current + "}";
        }
    }
}
