﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowerBox2.comands
{
    class DoorComand : Comand
    {
        private const byte DOOR = 3;
        private const byte SIZE = 2;
        private byte door;

        public DoorComand(byte door) {
            if (door == 0)
            {
                this.door = 0;
            }
            else
            {
                this.door = 1;
            }
        }

        public DoorComand(){}

        public override byte[] getComand()
        {
            byte[] bytes = new byte[2];
            bytes[0] = DOOR;
            bytes[1] = door;
            return bytes;
        }

        public override byte getSizeComand()
        {
            return SIZE;
        }

        public byte getDoor()
        {
            return door;
        }

        public void setDoor()
        {
            door = 1;
        }

        public override string ToString()
        {
            return "DoorComand{door: " + door + "}";
        }
    }
}
