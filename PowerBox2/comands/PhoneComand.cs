﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowerBox2.comands
{
    class PhoneComand : Comand
    {
        private const byte PHONE = 4;
        private const byte SIZE = 1;
        private byte phone;

        public PhoneComand(byte phone) {
            this.phone = phone;
        }

        public PhoneComand(){}

        public override byte[] getComand()
        {
            byte[] bytes = new byte[2];
            bytes[0] = PHONE;
            return bytes;
        }

        public override byte getSizeComand()
        {
            return SIZE;
        }

        public byte getPhone()
        {
            return phone;
        }

        public override string ToString()
        {
            return "PhoneComand{phone: " + phone + "}";
        }
    }
}
