﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowerBox2.comands
{
    class VoltageComand : Comand
    {
        private const byte VOLTAGE = 6;
        private const byte SIZE = 1;
        private int voltage;

        public VoltageComand(byte voltageLow, byte voltageHigh )
        {
            this.voltage = voltageLow + (voltageHigh * 255);
        }

        public VoltageComand() { }

        public override byte[] getComand()
        {
            byte[] bytes = new byte[2];
            bytes[0] = VOLTAGE;
            return bytes;
        }

        public override byte getSizeComand()
        {
            return SIZE;
        }

        public int getVoltage()
        {
            return voltage;
        }

        public override string ToString()
        {
            return "VoltageComand{voltage: " + voltage + "}";
        }
    }
}
