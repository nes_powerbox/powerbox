﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowerBox2.comands
{
    class LEDComand : Comand
    {
        private const byte LED = 2;
        private const byte SIZE = 2;
        private byte led = 0;

        public override byte[] getComand()
        {
            byte[] bytes = new byte[2];
            bytes[0] = LED;
            bytes[1] = led;
            return bytes;
        }

        public LEDComand(byte led)
        {
            if (led == 0)
            {
                this.led = 0;
            }
            else
            {
                this.led = 1;
            }
        }

        public LEDComand() { }

        public override byte getSizeComand()
        {
            return SIZE;
        }

        public void setLED(byte led)
        {
            if(led == 0)
            {
                this.led = 0;
            }
            else
            {
                this.led = 1;
            }       
        }

        public byte getLED()
        {
            return led;
        }

        public override string ToString()
        {
            return "LEDComand{led: " + led + "}";
        }
    }
}
