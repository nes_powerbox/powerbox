﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowerBox2.comands
{
    class PowerComand:Comand
    {
        private const byte POWER = 0;
        private const byte SIZE = 2;
        private byte power = 0;

        public PowerComand(byte power)
        {
            if (power == 0)
            {
                this.power = 0;
            }
            else
            {
                if (power == 1)
                {
                    this.power = 1;
                }
                else
                {
                    this.power = 2;
                }
            }
        }

        public PowerComand() { }

        public override byte[] getComand()
        {
            byte[] bytes = new byte[2];
            bytes[0] = POWER;
            bytes[1] = power;
            return bytes;
        }

        public override byte getSizeComand()
        {
            return SIZE;
        }

        public void setPower(byte power)
        {
            if (power == 0)
            {
                this.power = 0;
            }
            else
            {
                if (power == 1)
                {
                    this.power = 1;
                }
                else
                {
                    this.power = 2;
                }
            }
        }

        public byte getPower()
        {
            return power;
        }

        public override string ToString()
        {
            return "PowerComand{power: " + power + "}";
        }
    }
}
