﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowerBox2.comands
{
    abstract class Comand : IEquatable<Comand>
    {
        abstract public byte[] getComand();
        abstract public byte getSizeComand();

        public override int GetHashCode()
        {
            return getComand()[0];
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as Comand);
        }

        public bool Equals(Comand obj)
        {
            return obj != null && obj.getComand()[0] == this.getComand()[0];
        }

        //public bool Equals<T>(T obj) where T : Comand
        //{
        //    return obj != null && obj.getComand()[0] == this.getComand()[0];
        //}
    }
}
