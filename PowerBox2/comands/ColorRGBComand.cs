﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowerBox2.comands
{
    class ColorRGBComand : Comand
    {
        private const byte COLOR_RGB = 1;
        private const byte SIZE = 4;
        private byte colorR = 0;
        private byte colorG = 0;
        private byte colorB = 0;

        public ColorRGBComand(byte colorR, byte colorG, byte colorB)
        {
            this.colorR = colorR;
            this.colorG = colorG;
            this.colorB = colorB;
        }

        public ColorRGBComand(){}

        public override byte[] getComand()
        {
            byte[] bytes = new byte[4];
            bytes[0] = COLOR_RGB;
            bytes[1] = colorR;
            bytes[2] = colorG;
            bytes[3] = colorB;
            return bytes;
        }

        public override byte getSizeComand()
        {
            return SIZE;
        }

        public void setColorRGB(byte colorR, byte colorG, byte colorB)
        {
            this.colorR = colorR;
            this.colorG = colorG;
            this.colorB = colorB;
        }

        public byte getColorR()
        {
            return colorR;
        }

        public byte getColorG()
        {
            return colorG;
        }

        public byte getColorB()
        {
            return colorB;
        }

        public override string ToString()
        {
            return "ColorRGBComand{colorR: " + colorR + 
                "; colorG: " + colorG + 
                "; colorB: " + colorB + "}";
        }
    }
}
