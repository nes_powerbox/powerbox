﻿using System;
using System.Threading.Tasks;
using Windows.Devices.Enumeration;
using Windows.Devices.I2c;

namespace PowerBox2
{
    public static class I2C_Helper
    {
        private static string AQS;
        private static DeviceInformationCollection DIS;

        public static async Task<byte[]> WriteRead(int I2C_Slave_Address, byte[] comands)
        {
            byte[] response = new byte[21];
            try
            {
                // Initialize I2C
                var Settings = new I2cConnectionSettings(I2C_Slave_Address);
                Settings.BusSpeed = I2cBusSpeed.StandardMode;

                if (AQS == null || DIS == null)
                {
                    AQS = I2cDevice.GetDeviceSelector("I2C1");
                    DIS = await DeviceInformation.FindAllAsync(AQS);
                }
                using (I2cDevice Device = await I2cDevice.FromIdAsync(DIS[0].Id, Settings))
                {
                    Device.Write(comands);
                    Device.Read(response);
                }
            }
            catch (Exception ex)
            {
                RemovableFile.WriteSD_Log("I2C_Helper: Slave address(" + I2C_Slave_Address + ") Exception -> " + ex.Message);
            }
            return response;
        }

    }
}
