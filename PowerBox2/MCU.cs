﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PowerBox2
{//TODO задержка проверки открытия двери
    class MCU
    {
        private const int TIME_RESPON = 12000;

        private byte[] response;

        private int I2C_Slave_Address;

        //private I2C_Helper i2c = new I2C_Helper();

        private bool respon;

        private Task exception;

        private StatusMCU status;

        public enum StatusMCU : byte
        {
            WORKING = 0,
            NOT_WORKING = 1
        }

        private MySemaphore _pool = new MySemaphore(1, 1);

        public MCU(int I2C_Slave_Address)
        {
            respon = false;

            status = StatusMCU.WORKING;

            this.I2C_Slave_Address = I2C_Slave_Address;

            try
            {
                init();
            }
            catch (Exception ex)
            {
                status = StatusMCU.NOT_WORKING;
            }
        }

        public StatusMCU getStatus()
        {
            return status;
        }

        private async Task WriteRead(byte[] comands)
        {
            try
            {
                for (int i = 0; i < 20; i++)
                {
                    try {
                        response = await I2C_Helper.WriteRead(I2C_Slave_Address, comands);
                        break;
                    }
                    catch(Exception ex) {
                        if (i == 19) {
                            RemovableFile.WriteSD_Log("MCU: Exception->" + ex.Message);
                        }
                    }
                }
            }
            finally
            {
                respon = true;
            }
        }

        public int getI2C_Slave_Address()
        {
            return I2C_Slave_Address;
        }

        private void waitResponse(int time)
        {
            Task thread = new Task(() => {
                while (!respon) { }
            });
            thread.Start();
            thread.Wait(time);
            if (!respon)
            {
                throw new Exception("Box does not respond");
            }
            else
            {
                respon = false;
                if (!respon && exception.IsFaulted)
                {
                    exception = null;
                    RemovableFile.WriteSD_Log("MCU: Exception -> MCU with that address does not exist or the connection is lost with him");
                }
            }
        }

        public void setParams(HashSet<comands.Comand> comands)
        {
            _pool.Wait();
            try
            {
                while (true)
                {
                    Task thread = new Task(() => {
                        exception = WriteRead(generationRequestSet(comands));
                    });
                    thread.Start();
                    waitResponse(TIME_RESPON);

                    if (response[0] != 255)
                    {
                        break;
                    }
                }
            }
            catch(Exception ex)
            {
                RemovableFile.WriteSD_Log("MCU: Exception->" + ex.Message);
            }
            finally
            {
                _pool.TryRelease();
            }
        }

        public Dictionary<comands.Comand, comands.Comand> getParams(HashSet<comands.Comand> comands)
        {
            _pool.Wait();
            try
            {
                while (true)
                {
                    //RemovableFile.WriteSD_Log(I2C_Slave_Address + " load get params: " + string.Join(",", comands));
                    Task thread = new Task(() => {
                        exception = WriteRead(generationRequestGet(comands));
                    });
                    thread.Start();
                    waitResponse(TIME_RESPON);

                    if ((response[0] != 255) && (response[0] < 22) && (response[0] > 3))
                    {
                        byte checksum = 0;
                        for (int i = 0; i < response[0] - 1; i++)
                        {
                            checksum += response[i];
                        }

                        if (checksum == response[response[0] - 1])
                        {
                            break;
                        }
                    }
                }


                Dictionary<comands.Comand, comands.Comand> comandsAnswer = new Dictionary<comands.Comand, comands.Comand>();

                for (int i = 2; i < response[0]; i++)
                {
                    switch (response[i])
                    {
                        case 0:
                            {   //power
                                comands.PowerComand powerComand = new comands.PowerComand();
                                powerComand.setPower(response[i + 1]);
                                comandsAnswer.Add(powerComand, powerComand);
                                i += 2;
                            }
                            break;
                        case 1:
                            {    //color rgb
                                comands.ColorRGBComand colorRGBComand = new comands.ColorRGBComand();
                                colorRGBComand.setColorRGB(response[i + 1], response[i + 2], response[i + 3]);
                                comandsAnswer.Add(colorRGBComand, colorRGBComand);
                                i += 4;
                            }
                            break;
                        case 2:
                            {    //LED    
                                comands.LEDComand ledComand = new comands.LEDComand();
                                ledComand.setLED(response[i + 1]);
                                comandsAnswer.Add(ledComand, ledComand);
                                i += 2;
                            }
                            break;
                        case 3:
                            {    //open door    
                                comands.DoorComand doorComand = new comands.DoorComand(response[i + 1]);
                                comandsAnswer.Add(doorComand, doorComand);
                                i += 2;
                            }
                            break;
                        case 4:
                            {    //status_phone    
                                comands.PhoneComand phoneComand = new comands.PhoneComand(response[i + 1]);
                                comandsAnswer.Add(phoneComand, phoneComand);
                                i += 2;
                            }
                            break;
                        case 5:
                            {    //avarage_current    
                                comands.CurrentComand currentComand = new comands.CurrentComand(response[i + 1], response[i + 2]);
                                comandsAnswer.Add(currentComand, currentComand);
                                i += 3;
                            }
                            break;
                        case 6:
                            {    //avarage_voltage    
                                comands.VoltageComand voltageComand = new comands.VoltageComand(response[i + 1], response[i + 2]);
                                comandsAnswer.Add(voltageComand, voltageComand);
                                i += 3;
                            }
                            break;
                    };
                };
                return comandsAnswer;
            }
            catch(Exception ex)
            {
                RemovableFile.WriteSD_Log("MCU: Exception->" + ex.Message);
                return null;
            }
            finally {
                _pool.TryRelease();
            }
        }

        public comands.PhoneComand getPhone()
        {
            try
            {
                HashSet<comands.Comand> request = new HashSet<comands.Comand>();
                comands.PowerComand powerComand = new comands.PowerComand();
                request.Add(powerComand);
                powerComand = (comands.PowerComand)getParams(request)[powerComand];

                request.Clear();
                byte power = powerComand.getPower();
                if (power != 1) {
                    powerComand.setPower(1);
                    request.Add(powerComand);
                    setParams(request);
                }
                ////Task.Delay(-1).Wait(300);
                request.Clear();
                comands.PhoneComand phoneComand = new comands.PhoneComand();
                request.Add(phoneComand);
                phoneComand = (comands.PhoneComand)getParams(request)[phoneComand];

                if(power == 0)
                {
                    request.Clear();
                    powerComand.setPower(0);
                    request.Add(powerComand);
                    setParams(request);
                }

                if (power == 2)
                {
                    request.Clear();
                    powerComand.setPower(2);
                    request.Add(powerComand);
                    setParams(request);
                }

                return phoneComand;
            }
            catch(Exception ex)
            {
                RemovableFile.WriteSD_Log("MCU: Exception->" + ex.Message);
                return null;
            }
        }

        public comands.DoorComand getDoor()
        {
            try
            {
                HashSet<comands.Comand> request = new HashSet<comands.Comand>();
                comands.DoorComand doorComand = new comands.DoorComand();
                request.Add(doorComand);
                return (comands.DoorComand)getParams(request)[doorComand];
            }
            catch (Exception ex)
            {
                RemovableFile.WriteSD_Log("MCU: Exception->" + ex.Message);
                return null;
            }
        }

        public void setDoor(comands.DoorComand doorComand)
        {
            try
            {
                HashSet<comands.Comand> request = new HashSet<comands.Comand>();
                request.Add(doorComand);
                setParams(request);
            }
            catch (Exception ex)
            {
                RemovableFile.WriteSD_Log("MCU: Exception->" + ex.Message);
            }
        }

        public void setLED(comands.LEDComand ledComand) {
            try
            {
                HashSet<comands.Comand> request = new HashSet<comands.Comand>();
                request.Add(ledComand);
                setParams(request);
            }
            catch (Exception ex)
            {
                RemovableFile.WriteSD_Log("MCU: Exception->" + ex.Message);
            }
        }

        public void setRGB(comands.ColorRGBComand colorRGBComand)
        {
            try
            {
                HashSet<comands.Comand> request = new HashSet<comands.Comand>();
                request.Add(colorRGBComand);
                setParams(request);
            }
            catch (Exception ex)
            {
                RemovableFile.WriteSD_Log("MCU: Exception->" + ex.Message);
            }
        }

        public comands.PowerComand getPower()
        {
            try
            {
                HashSet<comands.Comand> request = new HashSet<comands.Comand>();
                comands.PowerComand powerComand = new comands.PowerComand();
                request.Add(powerComand);
                return (comands.PowerComand)getParams(request)[powerComand];
            }
            catch (Exception ex)
            {
                RemovableFile.WriteSD_Log("MCU: Exception->" + ex.Message);
                return null;
            }
        }

        public comands.LEDComand getLED()
        {
            try
            {
                HashSet<comands.Comand> request = new HashSet<comands.Comand>();
                comands.LEDComand ledComand = new comands.LEDComand();
                request.Add(ledComand);
                return (comands.LEDComand)getParams(request)[ledComand];
            }
            catch (Exception ex)
            {
                RemovableFile.WriteSD_Log("MCU: Exception->" + ex.Message);
                return null;
            }
        }

        public comands.ColorRGBComand getRGB()
        {
            try
            {
                HashSet<comands.Comand> request = new HashSet<comands.Comand>();
                comands.ColorRGBComand colorRGBComand = new comands.ColorRGBComand();
                request.Add(colorRGBComand);
                return (comands.ColorRGBComand)getParams(request)[colorRGBComand];
            }
            catch (Exception ex)
            {
                RemovableFile.WriteSD_Log("MCU: Exception->" + ex.Message);
                return null;
            }
        }

        public comands.CurrentComand getCurrent()
        {
            try
            {
                HashSet<comands.Comand> request = new HashSet<comands.Comand>();
                comands.PowerComand powerComand = new comands.PowerComand();
                request.Add(powerComand);
                powerComand = (comands.PowerComand)getParams(request)[powerComand];

                request.Clear();
                byte power = powerComand.getPower();
                if (power != 1)
                {
                    powerComand.setPower(1);
                    request.Add(powerComand);
                    setParams(request);
                }

                ////Task.Delay(-1).Wait(100);
                request.Clear();
                comands.CurrentComand currentComand = new comands.CurrentComand();
                request.Add(currentComand);
                currentComand = (comands.CurrentComand)getParams(request)[currentComand];

                if (power == 0)
                {
                    request.Clear();
                    powerComand.setPower(0);
                    request.Add(powerComand);
                    setParams(request);
                }

                if (power == 2)
                {
                    request.Clear();
                    powerComand.setPower(2);
                    request.Add(powerComand);
                    setParams(request);
                }
                return currentComand;
            }
            catch (Exception ex)
            {
                RemovableFile.WriteSD_Log("MCU: Exception->" + ex.Message);
                return null;
            }
        }

        public comands.VoltageComand getVoltage()
        {
            try
            {
                HashSet<comands.Comand> request = new HashSet<comands.Comand>();
                comands.PowerComand powerComand = new comands.PowerComand();
                request.Add(powerComand);
                powerComand = (comands.PowerComand)getParams(request)[powerComand];

                request.Clear();
                byte power = powerComand.getPower();
                if (power != 1)
                {
                    powerComand.setPower(1);
                    request.Add(powerComand);
                    setParams(request);
                }

                ////Task.Delay(-1).Wait(100);
                request.Clear();
                comands.VoltageComand voltageComand = new comands.VoltageComand();
                request.Add(voltageComand);
                voltageComand = (comands.VoltageComand)getParams(request)[voltageComand];

                if (power == 0)
                {
                    request.Clear();
                    powerComand.setPower(0);
                    request.Add(powerComand);
                    setParams(request);
                }

                if (power == 2)
                {
                    request.Clear();
                    powerComand.setPower(2);
                    request.Add(powerComand);
                    setParams(request);
                }
                return voltageComand;
            }
            catch (Exception ex)
            {
                RemovableFile.WriteSD_Log("MCU: Exception->" + ex.Message);
                return null;
            }
        }

        public void setPower(comands.PowerComand powerComand)
        {
            try
            {
                HashSet<comands.Comand> request = new HashSet<comands.Comand>();
                request.Add(powerComand);
                setParams(request);
            }
            catch (Exception ex)
            {
                RemovableFile.WriteSD_Log("MCU: Exception->" + ex.Message);
            }
        }

        private void init()
        {
            HashSet<comands.Comand> request = new HashSet<comands.Comand>();
            comands.PowerComand powerComand = new comands.PowerComand();
            request.Add(powerComand);
            powerComand = (comands.PowerComand)getParams(request)[powerComand];
            ////Task.Delay(-1).Wait(500);
            request.Add(new comands.PowerComand(0));
            request.Add(new comands.ColorRGBComand(0, 0, 0));
            request.Add(new comands.LEDComand(0));
            setParams(request);
    }

        public byte[] generationRequestSet(HashSet<comands.Comand> comands) {
            byte size = 0;
            foreach (comands.Comand comand in comands)
            {
                size += comand.getSizeComand();
            }

            size += 3;
            byte[] request = new byte[size];
            request[0] = size;
            request[1] = 0;

            byte i = 2;
            foreach (comands.Comand comand in comands)
            {
                foreach (byte value in comand.getComand())
                {
                    request[i] = value;
                    i++;
                }
            }

            byte checksum = 0;
            for (int j = 0; j < size; j++)
            {
                checksum += request[j];
            }

            request[request.Length - 1] = checksum;

            return request;
        }

        public byte[] generationRequestGet(HashSet<comands.Comand> comands)
        {
            byte size = (byte)(comands.Count + 3);

            byte[] request = new byte[size];
            request[0] = size;
            request[1] = 1;

            byte i = 2;
            foreach (comands.Comand comand in comands)
            {
                request[i] = comand.getComand()[0];
                i++;
            }

            byte checksum = 0;
            for (int j = 0; j < size - 1; j++)
            {
                checksum += request[j];
            }

            request[request.Length - 1] = checksum;

            return request;
        }

    }
}
