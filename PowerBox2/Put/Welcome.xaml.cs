﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Core;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Шаблон элемента пустой страницы задокументирован по адресу http://go.microsoft.com/fwlink/?LinkId=234238

namespace PowerBox2.Put
{
    /// <summary>
    /// Пустая страница, которую можно использовать саму по себе или для перехода внутри фрейма.
    /// </summary>
    public sealed partial class Welcome : Page
    {
        private Box box;
        private bool flag;

        private MySemaphore _pool = new MySemaphore(1, 2);
        private MySemaphore _pool2 = new MySemaphore(1, 2);

        private List<int> freeCells;


        public Welcome()
        {
            this.InitializeComponent();
            this.NavigationCacheMode = NavigationCacheMode.Enabled; //сохранение состояния страницы
            freeCells = new List<int>();
            flag = false;
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            Random rnd = new Random();

            if (freeCells.Count > 0)
            {
                int currentCell = rnd.Next(0, freeCells.Count);
                try
                {
                    _pool.Wait();
                    flag = true;
                    box.scaner.genExcept(); //генерация ошибки для освобождения потока
                }
                catch (Exception) { }
                finally
                {
                    _pool.TryRelease();
                }
                _pool2.Wait();
                _pool2.TryRelease();

                box.numberCell = freeCells[currentCell];
                box.privilege = FingerPrintScaner.Privilege.USER;
                //this.Frame.Navigate(typeof(PutDevice), box);
                //this.Frame.Navigate(typeof(MainPage), box);
                //this.Frame.Navigate(typeof(Pick_up.PickUpDevice), box);
                this.Frame.Navigate(typeof(Scanning), box);
             }
            else {
                textBlock.Text = "Нет свободных ячеек!";
            }
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (e.Parameter is Box)
            {
                box = (Box)e.Parameter;
            }
            base.OnNavigatedTo(e);
            textBlock.Text = "";

            FingerPrintScaner.Person[] user = null;         
            try
            {
                user = box.scaner.getUserNumbersAndPrivilege();
                for (int i = 0; i < box.mcu.Length; i++)
                {
                    if (box.mcu[i].getStatus() == MCU.StatusMCU.WORKING)
                    {
                        freeCells.Add(i);
                    }
                }

                for (int i = 0; i < user.Length; i++)
                {
                    if ((user[i].getID() < box.mcu.Length))
                    {
                        freeCells.Remove(user[i].getID());
                    }
                }

                if (freeCells.Count == 0)
                {
                    button.IsEnabled = false;
                }
                else
                {
                    button.IsEnabled = true;
                }

                Task thread = new Task(() =>
                {
                    scanning();
                });
                thread.Start();
            }
            catch (Exception ex)
            {
                textBlock.Text = ex.Message;
                return;
            }
        }

        private async void scanning()
        {
            FingerPrintScaner.Person user = null;
            while (true)
            {
                try
                {
                    _pool2.Wait();
                    user = box.scaner.compareOneToMore();
                    if (user.getPrivilege() == FingerPrintScaner.Privilege.ADMIN)
                    {
                        await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                        {
                            this.Frame.Navigate(typeof(MainPage), box);
                        });
                        return;
                    }
                    else
                    {
                        box.numberCell = user.getID();
                    }
                    _pool.Wait();
                    if (flag)
                    {
                        flag = false;
                        return;
                    }
                    break;
                }
                catch (Exception ex)
                {
                    if (flag)
                    {
                        flag = false;
                        return;
                    }
                    await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                    {
                        textBlock.Text = ex.Message;
                    });
                }
                finally
                {
                    _pool.TryRelease();
                    _pool2.TryRelease();
                }
            }

            await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                this.Frame.Navigate(typeof(Pick_up.PickUpDevice), box);
            });
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            try
            {
                _pool.Wait();
                flag = true;
                box.scaner.genExcept(); //генерация ошибки для освобождения потока
            }
            catch (Exception) { }
            finally
            {
                _pool.TryRelease();
            }
            _pool2.Wait();
            _pool2.TryRelease();
            textBlock.Text = "";
            this.Frame.Navigate(typeof(Instruction));
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            try
            {
                _pool.Wait();
                flag = true;
                box.scaner.genExcept(); //генерация ошибки для освобождения потока
            }
            catch (Exception) { }
            finally
            {
                _pool.TryRelease();
            }
            _pool2.Wait();
            _pool2.TryRelease();
            this.Frame.Navigate(typeof(Information));
        }
    }
}
