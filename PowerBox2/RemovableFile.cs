﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.Storage.Streams;

namespace PowerBox2
{
    public static class RemovableFile
    {
        private const string MAIN_FOLDER = "PowerBox";
        private const string CHECKS_FOLDER = "CHECKS";
        private const string WATCHING_FOLDER = "WATCHING";
        private const string LOG_FOLDER = "LOGGING";

        private const string LOG_FILE = "LOGGING_";
        private const string CHECK_FILE = "CHECK_";

        private static StorageFolder mainFolder;
        private static StorageFolder checksFolder;
        private static StorageFolder watchingFolder;
        private static StorageFolder logFolder;
        private static StorageFile logFile;
        private static StorageFile checkFile;
        private static int countCheck;
        private static int countLog;

        public enum ValueFolder : byte
        {
            mainFolder = 0,
            checksFolder = 1,
            watchingFolder = 2,
            logFolder = 3
        }

        public static void init()
        {
            var task = Task.Run(async () => { await initSD(); });
            task.Wait();
        }

        private static async Task initSD()
        {
            StorageFolder rootFolder = (await KnownFolders.RemovableDevices.GetFoldersAsync())
                .Single(e => e.DisplayName.Contains("POWERBOX"));

            if (rootFolder != null)
            {
                mainFolder = await createFolderInFolder(rootFolder, MAIN_FOLDER);
                checksFolder = await createFolderInFolder(mainFolder, CHECKS_FOLDER);
                watchingFolder = await createFolderInFolder(mainFolder, WATCHING_FOLDER);
                logFolder = await createFolderInFolder(mainFolder, LOG_FOLDER);

                //TODO countLog, countCheck
            }
            else
            {
                throw new Exception("No SD card is present");
            }
        }

        public static async void dellFolderSD(ValueFolder value)
        {
            switch (value)
            {
                case ValueFolder.mainFolder:
                    await mainFolder.DeleteAsync();
                    break;
                case ValueFolder.checksFolder:
                    await checksFolder.DeleteAsync();
                    break;
                case ValueFolder.watchingFolder:
                    await watchingFolder.DeleteAsync();
                    break;
                case ValueFolder.logFolder:
                    await logFolder.DeleteAsync();
                    break;
                default:
                    break;
            }          
        }

        private static async Task<StorageFolder> createFolderInFolder(StorageFolder rootFolder, string NameNewFolder)
        {
            if (!isFolderName(await rootFolder.GetFoldersAsync(), NameNewFolder))
            {
                return await rootFolder.CreateFolderAsync(NameNewFolder);
            }
            else
            {
                return await rootFolder.GetFolderAsync(NameNewFolder);
            }
        }

        private static bool isFolderName(IReadOnlyList<StorageFolder> listFolders, string nameFolder)
        {
            foreach (StorageFolder folders in listFolders)
            {
                if (folders.Name == nameFolder)
                {
                    return true;
                }
            }
            return false;
        }

        private static bool isFileName(IReadOnlyList<StorageFile> listFile, string nameFile)
        {
            foreach (StorageFile files in listFile)
            {
                if (files.Name == nameFile + ".jpg")
                {
                    return true;
                }
            }
            return false;
        }

        public static void WriteSD_CheckIn(string someTextData)
        {
            var task = Task.Run(async () => { await WriteSD("check in" + someTextData); });
            task.Wait();
        }

        public static void WriteSD_CheckOut(string someTextData)
        {
            var task = Task.Run(async () => { await WriteSD("check out" + someTextData); });
            task.Wait();
        }

        public static void WriteSD_CheckOpen(string someTextData)
        {
            var task = Task.Run(async () => { await WriteSD("check open" + someTextData); });
            task.Wait();
        }

        private static async Task WriteSD(string someTextData)
        {         
            string fileName = string.Format("{0}_{1:00}.{2:00}.{3}",
                CHECK_FILE,
                DateTime.Now.Day,
                DateTime.Now.Month,
                DateTime.Now.Year);

            checkFile = await checksFolder.CreateFileAsync(string.Format("{0}.jpg", fileName), 
                    CreationCollisionOption.OpenIfExists);

            using (var outputStream = await checkFile.OpenStreamForWriteAsync())
            {
                outputStream.Seek(0, SeekOrigin.End);
                DataWriter dataWriter = new DataWriter(outputStream.AsOutputStream());
                dataWriter.WriteString(string.Format("{0}_{1}:{2}:{3}{4}",
                    someTextData,
                    DateTime.Now.Hour,
                    DateTime.Now.Minute,
                    DateTime.Now.Second,
                    Environment.NewLine));
                await dataWriter.StoreAsync();
                await outputStream.FlushAsync();
                outputStream.Dispose();
            }
        }

        public static void WriteSD_Log(string someTextData)
        {
            var task = Task.Run(async () => { await WriteLogAsync(someTextData); });
            task.Wait();
        }

        private static async Task WriteLogAsync(string log)
        {
            string fileName = string.Format("{0}_{1:00}.{2:00}.{3}",
               LOG_FILE,
               DateTime.Now.Day,
               DateTime.Now.Month,
               DateTime.Now.Year);

            logFile = await logFolder.CreateFileAsync(string.Format("{0}.jpg", fileName),
                    CreationCollisionOption.OpenIfExists);

            using (var outputStream = await logFile.OpenStreamForWriteAsync())
            {
                outputStream.Seek(0, SeekOrigin.End);
                DataWriter dataWriter = new DataWriter(outputStream.AsOutputStream());
                dataWriter.WriteString(string.Format("{0}_{1:00}:{2:00}:{3:00}{4}",
                    log,
                    DateTime.Now.Hour,
                    DateTime.Now.Minute,
                    DateTime.Now.Second,
                    Environment.NewLine)
                    );
                await dataWriter.StoreAsync();
                await outputStream.FlushAsync();
                outputStream.Dispose();
            }
        }

        public static StorageFolder getFolderWatch()
        {
            return watchingFolder;
        }
    }
}
