﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Шаблон элемента пустой страницы задокументирован по адресу http://go.microsoft.com/fwlink/?LinkId=234238

namespace PowerBox2.Start
{
    /// <summary>
    /// Пустая страница, которую можно использовать саму по себе или для перехода внутри фрейма.
    /// </summary>
    public sealed partial class Authorization : Page
    {
        private Box box;

        public Authorization()
        {
            this.InitializeComponent();
            try
            {
                box = new Box();
                RemovableFile.WriteSD_Log("Init box successfully");
            }
            catch (Exception ex)
            {
                RemovableFile.WriteSD_Log("ex.Message");
                information.Text = ex.Message;
            }
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (box != null)
            {
                try
                {
                    string respon = box.scaner.setTimeoutValue(5);
                    if (respon != "Operation successfully")
                    {
                        RemovableFile.WriteSD_Log("Exception: " + respon);
                        throw new Exception(respon);
                    }
                }
                catch (Exception ex)
                {
                    RemovableFile.WriteSD_Log(ex.Message);
                }
                RemovableFile.WriteSD_Log("step scanning");
                Task thread = new Task(() =>
                {
                    scanning();
                });
                thread.Start();
                thread.Wait();
            }
            base.OnNavigatedTo(e);
        }

        protected override void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
            string respon = box.scaner.setTimeoutValue(0);
            if (respon != "Operation successfully")
            {
                RemovableFile.WriteSD_Log(respon);
            }
            base.OnNavigatingFrom(e);
        }

        private async void scanning()
        {
            try
            {
                //FingerPrintScaner.Person user = null;
                //try
                //{
                //    user = box.scaner.compareOneToMore();
                //}
                //catch (Exception e)
                //{
                //    box.debag.WriteSD_Debag(e.Message);
                //}
                //if (user != null && user.getPrivilege() == FingerPrintScaner.Privilege.ADMIN)
                //{
                //    await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                //    {
                //        this.Frame.Navigate(typeof(MainPage), box);
                //    });
                //}
                //else
                //{
                //}
                initCells();
                RemovableFile.WriteSD_Log("init cells");
                while (true)
                {
                    int n = isClosed();
                    if (n == 0)
                    {
                        break;
                    }
                    await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                    {
                        information.Text = "Ячейка №" + n + " не закрыта! Закройте пожалуйста.";
                    });
                    //Task.Delay(-1).Wait(1000);
                }


                await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                {
                    this.Frame.Navigate(typeof(Put.Welcome), box);
                });
            }
            catch (Exception ex)
            {
                RemovableFile.WriteSD_Log(ex.Message);
            }
        }

        private int isClosed()
        {
            //проверка закрытости ячеек
            for (int i = 0; i < box.mcu.Length; i++)
            {
                if (box.mcu[i].getStatus() == MCU.StatusMCU.WORKING) {
                    if (box.mcu[i].getDoor().getDoor() == 1)
                    {
                        return i + 1;
                    }
                }
            }
            return 0;
        }

        private void initCells()
        {
            for (int i = 0; i < box.mcu.Length; i++)
            {
                comands.PhoneComand phone = new comands.PhoneComand();
                phone = box.mcu[i].getPhone();
                //Task.Delay(-1).Wait(500);
                RemovableFile.WriteSD_Log("get status phone cell " + i);
                HashSet<comands.Comand> request = new HashSet<comands.Comand>();
                if (phone.getPhone() == 1)
                {
                    request.Add(new comands.ColorRGBComand(1, 0, 0));
                    request.Add(new comands.PowerComand(1));
                    RemovableFile.WriteSD_Log("set power on cell " + i);
                }
                else
                {
                    request.Add(new comands.ColorRGBComand(0, 1, 0));
                    RemovableFile.WriteSD_Log("set power off cell " + i);
                }
                box.mcu[i].setParams(request);
                //Task.Delay(-1).Wait(500);
                RemovableFile.WriteSD_Log("setup cell " + i);
            }
        }
    }
}
