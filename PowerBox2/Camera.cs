﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;
using Windows.Media.Capture;
using Windows.Media.MediaProperties;
using Windows.Storage;
using System.Threading.Tasks;
using Windows.Devices.Enumeration;
using System.Linq;
using Windows.Media.Core;
using System.Collections.Generic;
using Windows.Media.FaceAnalysis;

namespace PowerBox2
{
    class Camera
    {
        private MediaCapture mediaCapture;
        private StorageFile photoFile;
        private StorageFile recordStorageFile;
        private readonly string PHOTO_FILE_NAME = "photo.jpg";
        private readonly string VIDEO_FILE_NAME = "video.mp4";
        private bool isRecording;
        private DispatcherTimer timer;
        private bool isInitialized;
        private FaceDetectionEffect _faceDetectionEffect;
        private Action<int> delegateFaceDetect;

        public Camera()
        {
            isRecording = false;
            isInitialized = false;
            timer = new DispatcherTimer();
            timer.Tick += Timer_Tick;
        }

        public async void Cleanup()
        {
            printStatus("CleanupCameraAsync");
            if (isInitialized)
            {
                if (_faceDetectionEffect != null)
                {
                    CleanUpFaceDetectionEffectAsync();
                }

                if (timer.IsEnabled)
                {
                    timer.Stop();
                    timer.Tick -= Timer_Tick;
                    timer = null;
                }

                if (isRecording)
                {
                    await mediaCapture.StopRecordAsync();
                    isRecording = false;
                }
                await StopPreviewAsync();
                isInitialized = false;

                if (mediaCapture != null)
                {
                    mediaCapture.Dispose();
                    mediaCapture = null;
                }
            }
        }

        private void printStatus(string status)
        {
            RemovableFile.WriteSD_Log(status);
        }

        //'Initialize Audio and Video' button action function
        //Dispose existing MediaCapture object and set it up for audio and video
        //Enable or disable appropriate buttons
        //- DISABLE 'Initialize Audio and Video' 
        //- DISABLE 'Start Audio Record'
        //- ENABLE 'Initialize Audio Only'
        //- ENABLE 'Start Video Record'
        //- ENABLE 'Take Photo'
        public async void initVideo(Action<int> delegateFaceDetect)
        {
            this.delegateFaceDetect = delegateFaceDetect;
            try
            {
                if (mediaCapture != null)
                {
                    if (isRecording)
                    {
                        await mediaCapture.StopRecordAsync();
                        isRecording = false;
                    }
                    mediaCapture.Dispose();
                    mediaCapture = null;
                }

                // Attempt to get the front camera if one is available, but use any camera device if not
                var cameraDevice = await FindCameraDeviceByPanelAsync(Windows.Devices.Enumeration.Panel.Front);

                if (cameraDevice == null)
                {
                    printStatus("No camera device found");
                    return;
                }

                printStatus("Initializing camera to capture audio and video...");
                // Use default initialization
                mediaCapture = new MediaCapture();
                var settings = new MediaCaptureInitializationSettings { VideoDeviceId = cameraDevice.Id };

                // Initialize MediaCapture
                try
                {
                    await mediaCapture.InitializeAsync(settings);
                    isInitialized = true;
                }
                catch (UnauthorizedAccessException)
                {
                    printStatus("The app was denied access to the camera");
                }

                // Set callbacks for failure and recording limit exceeded
                printStatus("Device successfully initialized for video recording");
                mediaCapture.Failed += new MediaCaptureFailedEventHandler(mediaCapture_Failed);
                mediaCapture.RecordLimitationExceeded += new RecordLimitationExceededEventHandler(mediaCapture_RecordLimitExceeded);

                // If initialization succeeded, start the preview
                if (isInitialized)
                {
                    await StartPreviewAsync();
                }
                printStatus("Camera preview succeeded");
            }
            catch (Exception ex)
            {
                printStatus(ex.Message);
            }
        }

        private async Task StartPreviewAsync()
        {
            // Set the preview source in the UI and mirror it if necessary
            CaptureElement PreviewControl = new CaptureElement();
            PreviewControl.Source = mediaCapture;

            // Start the preview
            await mediaCapture.StartPreviewAsync();
        }

        private async Task StopPreviewAsync()
        {
            // Stop the preview
            await mediaCapture.StopPreviewAsync();
        }

        // 'Take Photo' button click action function
        // Capture image to a file in the default account photos folder
        public async void takePhoto()
        {
            if (isInitialized)
            {
                try
                {
                    photoFile = await RemovableFile.getFolderWatch().CreateFileAsync(PHOTO_FILE_NAME,
                        Windows.Storage.CreationCollisionOption.GenerateUniqueName);
                    ImageEncodingProperties imageProperties = ImageEncodingProperties.CreateJpeg();
                    await mediaCapture.CapturePhotoToStorageFileAsync(imageProperties, photoFile);
                    printStatus("Take Photo succeeded," + photoFile.Name);
                }
                catch (Exception ex)
                {
                    printStatus(ex.Message);
                    Cleanup();
                }
            }
            else
            {
                printStatus("Camera doesn't initialize");
            }
        }

        // 'Start Video Record' button click action function
        // Button name is changed to 'Stop Video Record' once recording is started
        // Records video to a file in the default account videos folder
        public async void recordVideo()
        {
            try
            {
                if (isInitialized)
                {
                    if (!isRecording)
                    {
                        printStatus("Initialize video recording");
                        recordStorageFile = await RemovableFile.getFolderWatch().CreateFileAsync(VIDEO_FILE_NAME,
                            Windows.Storage.CreationCollisionOption.GenerateUniqueName);

                        printStatus("Video storage file preparation successful," + recordStorageFile.Name);

                        MediaEncodingProfile recordProfile = null;
                        recordProfile = MediaEncodingProfile.CreateMp4(VideoEncodingQuality.Auto);

                        await mediaCapture.StartRecordToStorageFileAsync(recordProfile, recordStorageFile);

                        isRecording = true;
                        printStatus("Video recording in progress... press 'Stop Video Record' to stop");
                    }
                    else
                    {
                        printStatus("Stopping video recording...");
                        await mediaCapture.StopRecordAsync();
                        isRecording = false;
                    }
                }
                else
                {
                    printStatus("Camera doesn't initialize");
                }
            }
            catch (Exception ex)
            {
                if (ex is UnauthorizedAccessException)
                {
                    printStatus("Unable to play recorded video, video recorded successfully");
                }
                else
                {
                    printStatus(ex.Message);
                    Cleanup();
                }
            }
        }



        // Callback function for any failures in MediaCapture operations
        private async void mediaCapture_Failed(MediaCapture currentCaptureObject, MediaCaptureFailedEventArgs currentFailure)
        {
            await Task.Run(async () => {
                try
                {
                    printStatus("MediaCaptureFailed, " + currentFailure.Message);

                    if (isRecording)
                    {
                        await mediaCapture.StopRecordAsync();
                        printStatus("Recording Stopped");
                    }
                }
                catch (Exception)
                {
                }
                finally
                {
                    printStatus("Check if camera is diconnected. Try re-launching the app");
                }
            });
        }

        // Callback function if Recording Limit Exceeded
        private async void mediaCapture_RecordLimitExceeded(MediaCapture currentCaptureObject)
        {
            try
            {
                if (isRecording)
                {
                    await Task.Run(async() => {
                        try
                        {
                            printStatus("Stopping Record on exceeding max record duration");
                            await mediaCapture.StopRecordAsync();
                            isRecording = false;
                            printStatus("Stopped record on exceeding max record duration");
                        }
                        catch (Exception e)
                        {
                            printStatus(e.Message);
                        }
                    });
                }
            }
            catch (Exception e)
            {
                printStatus(e.Message);
            }
        }

        public void takePhotoFrequency(int frequency)
        {
            if (isInitialized)
            {
                if (timer.IsEnabled)
                {
                    timer.Stop();
                }
                else
                {
                    timer.Interval = TimeSpan.FromMilliseconds(frequency * 1000);
                    timer.Start();
                }
            }
            else
            {
                printStatus("Camera doesn't initialize");
            }
        }

        private void Timer_Tick(object sender, object e)
        {
            takePhoto();
        }

        public async void addFaceDetectionEffect()
        {
            if (isInitialized)
            {
                if (_faceDetectionEffect == null || !_faceDetectionEffect.Enabled)
                {
                    await CreateFaceDetectionEffectAsync();
                }
                else
                {
                    CleanUpFaceDetectionEffectAsync();
                }
            }
            else
            {
                printStatus("Camera doesn't initialize");
            }
        }

        private static async Task<DeviceInformation> FindCameraDeviceByPanelAsync(Windows.Devices.Enumeration.Panel desiredPanel)
        {
            // Get available devices for capturing pictures
            var allVideoDevices = await DeviceInformation.FindAllAsync(DeviceClass.VideoCapture);

            // Get the desired camera by panel
            DeviceInformation desiredDevice = allVideoDevices.FirstOrDefault(x => x.EnclosureLocation != null && x.EnclosureLocation.Panel == desiredPanel);

            // If there is no device mounted on the desired panel, return the first device found
            return desiredDevice ?? allVideoDevices.FirstOrDefault();
        }

        private async Task CreateFaceDetectionEffectAsync()
        {
            // Create the definition, which will contain some initialization settings
            var definition = new FaceDetectionEffectDefinition();

            // To ensure preview smoothness, do not delay incoming samples
            definition.SynchronousDetectionEnabled = false;

            // In this scenario, choose detection speed over accuracy
            definition.DetectionMode = FaceDetectionMode.HighPerformance;

            // Add the effect to the preview stream
            _faceDetectionEffect = (FaceDetectionEffect)await mediaCapture.AddVideoEffectAsync(definition, MediaStreamType.VideoPreview);

            // Register for face detection events
            _faceDetectionEffect.FaceDetected += FaceDetectionEffect_FaceDetected;

            // Choose the shortest interval between detection events
            _faceDetectionEffect.DesiredDetectionInterval = TimeSpan.FromMilliseconds(33);

            // Start detecting faces
            _faceDetectionEffect.Enabled = true;
        }

        private void CleanUpFaceDetectionEffectAsync()
        {
            // Disable detection
            _faceDetectionEffect.Enabled = false;

            // Unregister the event handler
            _faceDetectionEffect.FaceDetected -= FaceDetectionEffect_FaceDetected;

            // Clear the member variable that held the effect instance
            _faceDetectionEffect = null;
        }

        private async void FaceDetectionEffect_FaceDetected(FaceDetectionEffect sender, FaceDetectedEventArgs args)
        {
            await Task.Run(() => HighlightDetectedFaces(args.ResultFrame.DetectedFaces));
        }

        private void HighlightDetectedFaces(IReadOnlyList<DetectedFace> faces)
        {
            delegateFaceDetect(faces.Count);
        }

    }
}
