﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Шаблон элемента пустой страницы задокументирован по адресу http://go.microsoft.com/fwlink/?LinkId=234238

namespace PowerBox2.Pick_up
{
    /// <summary>
    /// Пустая страница, которую можно использовать саму по себе или для перехода внутри фрейма.
    /// </summary>
    public sealed partial class PickUpDevice : Page
    {
        private Box box;
        private bool cancel;
        private bool powerFailure;
        private bool phoneIsConnected;
        private bool absentUser;
        private Type previousPageType;

        private bool answer;

        public PickUpDevice()
        {
            this.InitializeComponent();
            this.NavigationCacheMode = NavigationCacheMode.Enabled; //сохранение состояния страницы
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            previousPageType = Frame.BackStack.Last().SourcePageType;
            App.Current.IsIdleChanged += onIsIdleChanged;

            if (previousPageType != typeof(Start.AreYouStillHere))
            {
                if (e.Parameter is Box)
                {
                    box = (Box)e.Parameter;
                }
                base.OnNavigatedTo(e);

                cancel = false;
                powerFailure = false;
                phoneIsConnected = false;
                answer = false;
                absentUser = true;

                HashSet<comands.Comand> request = new HashSet<comands.Comand>();
                request.Add(new comands.DoorComand(1));
                request.Add(new comands.LEDComand(1));
                request.Add(new comands.ColorRGBComand(0, 0, 1));
               // request.Add(new comands.PowerComand(1)); //TODO for test
                box.mcu[box.numberCell].setParams(request);

                RemovableFile.WriteSD_CheckOut(box.numberCell.ToString());
                Task thread = new Task(() =>
                {
                    waitingClosing();
                });
                thread.Start();
            }
            else
            {
                if (e.Parameter is bool)
                {
                    answer = (bool)e.Parameter;
                }

                absentUser = true;
                if (!answer)
                {
                    Task threadIdle = new Task(() =>
                    {
                        waitingClosingIdle();
                    });
                    threadIdle.Start();
                }
                else
                {
                    App.Current.onCoreWindowPointerMoved(null, null);
                    Task thread = new Task(() =>
                    {
                        waitingClosing();
                    });
                    thread.Start();
                }
            }

            
        }

        private void waitingClosingIdle()
        {
            if (phoneConnected())
            {
                do
                {
                    if (box.mcu[box.numberCell].getDoor().getDoor() == 0)
                    {
                        success();
                        dispatch(() => { this.Frame.Navigate(typeof(Goodbye), box); });
                        return;
                    }
                    dispatch(() => { textBlock1.Text = "Закройте ячейку!"; });
                    Task.Delay(-1).Wait(500);
                } while (phoneConnected());

                cancel = true;
                dispatch(() => { App.Current.onCoreWindowPointerMoved(null, null); });
                RemovableFile.WriteSD_CheckOut(box.numberCell.ToString() + "Phone was stolen");
                Task thread = new Task(() =>
                {
                    waitingClosing();
                });
                thread.Start();
            }
            else
            {
                annulment();
                dispatch(() => { this.Frame.Navigate(typeof(Goodbye), box); });
            }
        }

        private void waitingClosing()
        {
            try
            {
                back:
                while (absentUser)
                {
                    showPhoneStatus();
                    Task.Delay(-1).Wait(600);
                    while (cancel/* || powerFailure*/)
                    {
                        //if (powerFailure)
                        //{
                        //    box.mcu[box.numberCell].setPower(new comands.PowerComand(1));
                        //    Task.Delay(-1).Wait(300);
                        //    emergencyPowerOff();
                        //}

                        if (box.mcu[box.numberCell].getDoor().getDoor() == 0)
                        {
                            if (!phoneConnected())
                            {
                                annulment();
                                dispatch(() => { this.Frame.Navigate(typeof(Goodbye), box); });
                                return;
                            }
                            else
                            {
                                box.mcu[box.numberCell].setDoor(new comands.DoorComand(1));
                            }
                        }
                        goto back;
                    }

                    if (phoneConnected())
                    {
                        emergencyPowerOff();

                        if (powerFailure)
                        {
                            if (box.mcu[box.numberCell].getDoor().getDoor() == 0)
                            {
                                box.mcu[box.numberCell].setDoor(new comands.DoorComand(1));
                            }
                            continue;
                        }
                    }
                    else
                    {
                        if (box.mcu[box.numberCell].getDoor().getDoor() == 0)
                        {
                            annulment();
                            dispatch(() => { this.Frame.Navigate(typeof(Goodbye), box); });
                            return;
                        }
                        continue;                    
                    }


                    if (box.mcu[box.numberCell].getDoor().getDoor() == 0)
                    {
                        success();
                        dispatch(() => { this.Frame.Navigate(typeof(Put.Welcome), box); });
                        return;
                    }

                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void success()
        {
            HashSet<comands.Comand> request = new HashSet<comands.Comand>();
            comands.LEDComand ledComand = new comands.LEDComand(0);
            comands.ColorRGBComand colorRGBComand = new comands.ColorRGBComand(1, 0, 0);
            request.Add(ledComand);
            request.Add(colorRGBComand);
            box.mcu[box.numberCell].setParams(request);
        }

        private void annulment()
        {
            HashSet<comands.Comand> request = new HashSet<comands.Comand>();
            comands.LEDComand ledComand = new comands.LEDComand(0);
            comands.PowerComand powerComand = new comands.PowerComand(0);
            comands.ColorRGBComand colorRGBComand = new comands.ColorRGBComand(0, 1, 0);
            request.Add(ledComand);
            request.Add(powerComand);
            request.Add(colorRGBComand);
            box.mcu[box.numberCell].setParams(request);
            box.scaner.deleteUser(box.numberCell);
        }

        private void emergencyPowerOff()
        {
            box.mcu[box.numberCell].setPower(new comands.PowerComand(1));
            Task.Delay(-1).Wait(300);
            if (box.mcu[box.numberCell].getPower().getPower() != 2)
            {
                if (powerFailure)
                {
                    powerFailure = false;
                }
            }
            else
            {
                powerFailure = true;
            }
        }

        private bool phoneConnected()
        {
            if (box.mcu[box.numberCell].getPhone().getPhone() == 1)
            {
                phoneIsConnected = true;
                return true;
            }
            else
            {
                phoneIsConnected = false;
                return false;
            }
        }

       

        private void showPhoneStatus()
        {
            string stutus = "";
            if (powerFailure)
            {
                stutus += "Ошибка питания. Ваше устройство слишком много потребляет или неисправно!";
            }

            if (cancel)
            {
                if (phoneIsConnected)
                {
                    stutus += " Отключите телефон и";
                }
                stutus += " Закройте ячейку!";
            }

            if (!cancel && !powerFailure)
            {
                if (phoneIsConnected)
                {
                    stutus += "Ваше устройство заряжается! Закройте пожалуйста ячейку";
                }
                else
                {
                    stutus += "Продолжите зарядку или закройте ячейку!";
                }
            }

            dispatch(() => { textBlock1.Text = stutus; });
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            absentUser = false;
            dispatch(() => { textBlock1.Text = ""; });
            App.Current.IsIdleChanged -= onIsIdleChanged;
        }

        private void onIsIdleChanged(object sender, EventArgs e)
        {
            dispatch(() => { this.Frame.Navigate(typeof(Start.AreYouStillHere)); });
        }

        private async void dispatch(DispatchedHandler agileCallback)
        {
            await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, agileCallback);
        }
    }
}
